# taxesmemo

A tax tracking web application in which you can add taxes with or without contributors. Specially designed for students or people who share the apartment or house with several other people. It&#39;s main purpose is to monitor which users contributed to specific taxes and remind them by email if due date is near.