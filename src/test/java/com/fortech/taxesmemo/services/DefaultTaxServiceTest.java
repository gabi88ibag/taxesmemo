package com.fortech.taxesmemo.services;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fortech.taxesmemo.config.PersistenceContext;
import com.fortech.taxesmemo.entities.Tax;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { PersistenceContext.class })
class DefaultTaxServiceTest {

/*	@Autowired
	private TestEntityManager entityManager;*/
	
	@Autowired
	TaxService defaultTaxService;


	@Before
	public void setUp() {
		Tax tax = new Tax("UPC", 56.66, "2018-05-04");
		defaultTaxService.save(tax);

	}

	@Test
	void testGet() {

	}

	@Test
	void testList() {
	}

	@Test
	void testSave() {
	
		
	}

	@Test
	void testDelete() {
	}

}
