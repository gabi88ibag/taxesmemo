package com.fortech.taxesmemo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.fortech.taxesmemo.entities.Tax;

@Repository
public interface TaxRepository extends JpaRepository<Tax, Long> 	{

	List<Tax> findByNameContaining(String name);
	List<Tax> findByName(String name);

	@Query("SELECT u FROM User u JOIN FETCH u.taxes ut WHERE ut.id= :idUser")
	List<Tax> findTaxByIdUser(@Param("idUser") Long idUser);
}
