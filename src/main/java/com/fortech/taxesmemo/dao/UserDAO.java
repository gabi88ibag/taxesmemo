package com.fortech.taxesmemo.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.jdbc.ReturningWork;
import org.hibernate.jdbc.Work;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fortech.taxesmemo.entities.Tax;
import com.fortech.taxesmemo.entities.User;
import com.fortech.taxesmemo.pojo.TaxContributor;
import com.fortech.taxesmemo.util.HibernateUtil;
import com.fortech.taxesmemo.util.ListUtil;

@Repository
public class UserDAO implements CrudInterface<User> {

	@Autowired
	private UserRepository userRepository;

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Optional<User> findById(Long byId) {
		return userRepository.findById(byId);
	}

	public Optional<User> findUserByEmail(String email, String password) {
		return userRepository.findUserByEmailAndPassword(email, password);
	}
	
	public Optional<User> getByEmail(String email) {
		return userRepository.findByEmail(email);
	}

	public boolean findbyEmail(String email) {
		String query = "SELECT id_user from user where email = '" + email + "';";

		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		List<Long> usersId = session.doReturningWork(new ReturningWork<List<Long>>() {
			@Override
			public List<Long> execute(Connection connection) throws SQLException {
				List<Long> usersId = new ArrayList<>();
				Statement st = connection.createStatement();
				ResultSet rs = st.executeQuery(query);
				while (rs.next()) {

					usersId.add(rs.getLong(1));

					System.out.println("userId to add: " + rs.getLong(1));
				}
				return usersId;
			}
		});

		session.close();
		if (usersId.size() > 0) {
			return true;
		}
		return false;
	}

	public List<Tax> findTaxesByUserId(Long id) {
		String byUserId = "SELECT t.id_tax AS taxid, t.tax_name, t.price, t.tax_due_date FROM tax t JOIN user_tax ut ON  ut.id_user = '"
				+ id + "' AND ut.id_tax = t.id_tax;";

		return getTaxesByQuery(byUserId);
	}

	public List<Tax> findByUserIdAndTaxName(Long id, String name) {
		String byIdAndName = "SELECT t.id_tax AS taxid, t.tax_name, t.price, t.tax_due_date FROM tax t JOIN user_tax ut ON  ut.id_user = '"
				+ id + "' AND ut.id_tax = t.id_tax AND LOWER(t.tax_name) LIKE '%" + name + "%'";

		return getTaxesByQuery(byIdAndName);
	}

	private List<Tax> getTaxesByQuery(String query) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		List<Tax> taxes = session.doReturningWork(new ReturningWork<List<Tax>>() {
			@Override
			public List<Tax> execute(Connection connection) throws SQLException {
				Statement st = connection.createStatement();
				ResultSet rs = st.executeQuery(query);
				List<Tax> taxes = new ArrayList<>();
				while (rs.next()) {
					Tax tax = new Tax();
					System.out.println("tax id: " + rs.getLong(1));
					tax.setId(rs.getLong(1));
					tax.setName(rs.getString(2));
					tax.setPrice(rs.getDouble(3));
					tax.setDueDate(rs.getString(4));
					System.out.println("tax to add: " + tax.toString());
					taxes.add(tax);
				}
				return taxes;
			}
		});

		for (Tax t : taxes) {
			System.out.println(t.toString() + " added.");
		}
		session.close();

		return taxes;
	}

	private List<Long> getTaxesIdForUser(Long userId) {
		String query = "SELECT t.id_tax FROM tax t JOIN user_tax ut ON ut.id_tax = t.id_tax and ut.id_user = " + userId
				+ ";";

		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		List<Long> taxesId = session.doReturningWork(new ReturningWork<List<Long>>() {
			@Override
			public List<Long> execute(Connection connection) throws SQLException {
				Statement st = connection.createStatement();
				ResultSet rs = st.executeQuery(query);
				List<Long> taxesId = new ArrayList<>();
				while (rs.next()) {

					taxesId.add(rs.getLong(1));

					System.out.println("tax to add: " + rs.getLong(1));
				}
				return taxesId;
			}
		});

		for (Long id : taxesId) {
			System.out.println(id + " added.");
		}
		session.close();

		return taxesId;
	}

	public List<TaxContributor> getTaxContributors(Long userId) {
		List<Long> taxesId = getTaxesIdForUser(userId);
		String taxesIdString = ListUtil.separateWithComma(taxesId);

		System.out.println("taxes string: " + taxesIdString);

		String query = "SELECT u.id_user, u.first_name, u.last_name, t.tax_name, t.price, t.tax_due_date FROM user u "
				+ "JOIN user_tax ut ON  ut.id_user = u.id_user " + "JOIN tax t on ut.id_tax = t.id_tax "
				+ "WHERE (find_in_set(t.id_tax, '" + taxesIdString + "')) AND ut.tax_payed='0'";

		List<TaxContributor> contributors = queryForContributors(query);

		int contributorsNumber = getContributorsNumber(taxesId.get(0));
		for (TaxContributor tc : contributors) {
			tc.setTaxContributors(contributorsNumber);
		}

		return contributors;
	}

	private int getContributorsNumber(Long id) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		System.out.println("for contributor with id: " + id);
		String query = "select count(u) from User u JOIN u.taxes t where t.id ='" + id + "'";
		int count = ((Long) session.createQuery(query).uniqueResult()).intValue();
		return count;
	}

	public List<TaxContributor> getTaxContributorsByName(Long userId, String name) {
		List<Long> taxesId = getTaxesIdForUser(userId);
		String taxesIdString = ListUtil.separateWithComma(taxesId);

		System.out.println("taxes string: " + taxesIdString);

		String query = "SELECT u.id_user, u.first_name, u.last_name, t.tax_name, t.price, t.tax_due_date FROM user u "
				+ "JOIN user_tax ut ON  ut.id_user = u.id_user " + "JOIN tax t on ut.id_tax = t.id_tax "
				+ "WHERE (find_in_set(t.id_tax, '" + taxesIdString + "')) " + "AND LOWER(t.tax_name) LIKE '%" + name
				+ "%' AND ut.tax_payed='0'";

		return queryForContributors(query);
	}

	private List<TaxContributor> queryForContributors(String query) {
		Session session = HibernateUtil.getSessionFactory().openSession();

		session.beginTransaction();
		List<TaxContributor> taxContributors = session.doReturningWork(new ReturningWork<List<TaxContributor>>() {
			@Override
			public List<TaxContributor> execute(Connection connection) throws SQLException {
				Statement st = connection.createStatement();
				ResultSet rs = st.executeQuery(query);
				List<TaxContributor> taxContributors = new ArrayList<>();
				while (rs.next()) {
					TaxContributor tc = new TaxContributor();

					tc.setId(rs.getLong(1));
					tc.setFirstName(rs.getString(2));
					tc.setLastName(rs.getString(3));
					tc.setTaxName(rs.getString(4));
					tc.setPrice(rs.getDouble(5));
					tc.setDueDate(rs.getString(6));
					System.out.println("tax to add: " + tc.getId());
					taxContributors.add(tc);
				}
				return taxContributors;
			}
		});

		for (TaxContributor tc : taxContributors) {
			System.out.println(tc.toString() + " added.");
		}
		session.close();

		return taxContributors;
	}

	private List<User> queryForUsers(String query) {
		Session session = HibernateUtil.getSessionFactory().openSession();

		session.beginTransaction();
		List<User> users = session.doReturningWork(new ReturningWork<List<User>>() {
			@Override
			public List<User> execute(Connection connection) throws SQLException {
				Statement st = connection.createStatement();
				ResultSet rs = st.executeQuery(query);
				List<User> users = new ArrayList<>();
				while (rs.next()) {
					User u = new User();

					u.setIdUser(rs.getLong(1));
					u.setFirstName(rs.getString(2));
					u.setLastName(rs.getString(3));
					System.out.println("user to add: " + u.getIdUser());
					if (!users.contains(u)) {
						users.add(u);
					}
				}
				return users;
			}
		});

		for (User u : users) {
			System.out.println(u.toString() + " added.");
		}
		session.close();

		return users;
	}

	@Override
	public Collection<User> findAll() {
		return userRepository.findAll();
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public User save(User user) {
		System.out.println("user to save: " + user.getIdUser() + " " + user.toString());
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		session.save(user);
		session.getTransaction().commit();
		System.out.println("created with userId: " + user.getIdUser());
		return user;
	}

	public User update(User user) {
		entityManager.merge(user);
		return user;
	}

	@Override
	public void delete(User user) {
		for (Tax t : user.getTaxes()) {
			t.getUsers().remove(user);
		}
		entityManager.remove(user);
		entityManager.flush();
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void saveTax(Long userId, Tax tax) {
		User user = entityManager.find(User.class, userId);
		entityManager.persist(tax);
		user.addTax(tax);
		entityManager.persist(user);

		Set<TaxContributor> contributors = new HashSet<TaxContributor>(getTaxContributors(userId));
		for (TaxContributor tc : contributors) {
			User u = entityManager.find(User.class, tc.getId());
			entityManager.persist(tax);
			u.addTax(tax);
			entityManager.persist(u);
		}
		entityManager.flush();
	}

	public void updatePayed(Long taxId, Long id) {
		String query = "UPDATE user_tax SET tax_payed = 1 WHERE id_tax ='" + taxId + "' AND id_user ='" + id + "';";
		System.out.println(query);
		Session session = null;

		try {
			session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.doWork(new Work() {

				@Override
				public void execute(Connection connection) throws SQLException {
					Statement st = connection.createStatement();
					st.executeUpdate(query);
				}
			});
			session.getTransaction().commit();
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public List<User> getMyTaxContributors(Long id) {
		List<Long> taxesId = getTaxesIdForUser(id);

		String taxesIdString = ListUtil.separateWithComma(taxesId);

		System.out.println("taxes string: " + taxesIdString);

		String query = "SELECT u.id_user, u.first_name, u.last_name FROM user u "
				+ "JOIN user_tax ut ON  ut.id_user = u.id_user " + "JOIN tax t on ut.id_tax = t.id_tax "
				+ "WHERE (find_in_set(t.id_tax, '" + taxesIdString + "')) ";

		return queryForUsers(query);
	}
}
