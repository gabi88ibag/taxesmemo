package com.fortech.taxesmemo.dao;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.fortech.taxesmemo.entities.Tax;
import com.fortech.taxesmemo.entities.User;

@Repository
public class TaxDAO implements CrudInterface<Tax> {
	@Autowired
	private TaxRepository taxRepository;

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Optional<Tax> findById(Long byId) {
		return taxRepository.findById(byId);
	}

	public List<Tax> findByName(String name) {
		return taxRepository.findByName(name);
	}

	public List<Tax> findTaxByIdUser(Long id) {

		return taxRepository.findTaxByIdUser(id);
	}

	@Override
	public Collection<Tax> findAll() {
		return taxRepository.findAll();
	}

	@Override
	@Transactional
	public Tax save(Tax tax) {
		Optional<Tax> optionalTax = findById(tax.getId());
		if (optionalTax.isPresent()) {
			entityManager.merge(tax);
		} else {
			entityManager.persist(tax);
		}

		return tax;
	}

	@Transactional
	@Override
	public void delete(Tax tax) {
		entityManager.find(Tax.class, tax.getId());
		entityManager.remove(tax);

		for (User user : tax.getUsers()) {
			user.getTaxes().remove(tax);
		}
		entityManager.flush();
	}

	/**
	 * @param taxRepository
	 */
	public TaxDAO(TaxRepository taxRepository) {
		super();
		this.taxRepository = taxRepository;
	}

	public TaxRepository getTaxRepository() {
		return taxRepository;
	}

	public void setTaxRepository(TaxRepository taxRepository) {
		this.taxRepository = taxRepository;
	}
}
