package com.fortech.taxesmemo.dao;

import java.util.Collection;
import java.util.Optional;

public interface CrudInterface<T> {
	Optional<T> findById(Long byId);

	Collection<T> findAll();

	T save(T entity);

	void delete(T entity);

}
