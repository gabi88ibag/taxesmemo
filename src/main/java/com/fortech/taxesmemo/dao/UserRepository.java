package com.fortech.taxesmemo.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fortech.taxesmemo.entities.Tax;
import com.fortech.taxesmemo.entities.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	Optional<User> findUserByEmailAndPassword(String email, String password);

	Optional<User> findById(Long id);

	Optional<User> findByEmail(String email);

	List<Tax> findTaxByIdUser(Long idUser);

}
