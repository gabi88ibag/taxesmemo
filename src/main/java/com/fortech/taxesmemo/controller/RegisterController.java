package com.fortech.taxesmemo.controller;

import javax.servlet.http.HttpSession;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.ocpsoft.rewrite.annotation.Join;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fortech.taxesmemo.entities.User;
import com.fortech.taxesmemo.pojo.UserRole;
import com.fortech.taxesmemo.services.UserService;
import com.fortech.taxesmemo.util.PasswordUtil;
import com.fortech.taxesmemo.util.SessionUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;

@Component(value = "registerController")
@ELBeanName(value = "registerController")
@Join(path = "/register", to = "/register.jsf")
public class RegisterController extends LoginController {

	private User user = new User();
	private String confirmPassword;

	@Autowired
	private UserService userService;

	public String saveUser(User user) {

		user.setPassword(PasswordUtil.hashPassword(user.getPassword()));
		System.out.println("user to register: " + user.getFirstName());
		user.setRole(UserRole.ADMIN.getRole());
		System.out.println(UserRole.ADMIN.getRole());
		System.out.println("password: " + user.getPassword() + " confirmation pass: " + confirmPassword);

		String postURL = "http://localhost:8080/rest/users/create";

		ClientConfig clientConfig = new DefaultClientConfig();
		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		Client client = Client.create(clientConfig);

		WebResource webResourcePost = client.resource(postURL);

		ClientResponse response = webResourcePost.type("application/json").post(ClientResponse.class, user);

		System.out.println("client response: " + response.toString());
		if (response.getStatus() == 200) {

			String responseString = response.getEntity(String.class);
			System.out.println(responseString);
			user = new User();
			try {
				JSONObject object = new JSONObject(responseString);

				user.setIdUser(object.getLong("idUser"));
				user.setFirstName(object.getString("firstName"));
				user.setLastName(object.getString("lastName"));
				user.setRole(object.getString("role"));
				user.setEmail(object.getString("email"));

			} catch (JSONException e) {
				e.printStackTrace();
			}

			HttpSession session = SessionUtil.getSession();
			session.setAttribute("firstName", user.getFirstName());
			session.setAttribute("lastName", user.getLastName());
			session.setAttribute("email", user.getEmail());
			session.setAttribute("userId", user.getIdUser());
			session.setAttribute("role", user.getRole());

			return "/admin/admin-taxes.xhtml?faces-redirect=true;";

		}

		return null;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
