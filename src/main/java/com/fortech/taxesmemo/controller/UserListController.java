package com.fortech.taxesmemo.controller;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.ocpsoft.rewrite.annotation.Join;
import org.ocpsoft.rewrite.annotation.RequestAction;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.ocpsoft.rewrite.faces.annotation.Deferred;
import org.ocpsoft.rewrite.faces.annotation.IgnorePostback;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.fortech.taxesmemo.pojo.TaxContributor;
import com.fortech.taxesmemo.util.SessionUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;

@Scope(value = "session")
@Component(value = "userListController")
@ELBeanName(value = "userListController")
@Join(path = "/users", to = "/admin/user-list.jsf")
public class UserListController {

	private List<TaxContributor> taxContributors;
	private Long userId;
	private String searchString;

	private TaxContributor taxContributor;

	public List<TaxContributor> getUsers() {
		return taxContributors;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {

		this.userId = userId;
	}

	@Deferred
	@RequestAction
	@IgnorePostback
	public void listUsersByUserId() {
		userId = SessionUtil.getUserId();
		System.out.println("userId : " + userId);

		String path = "http://localhost:8080/rest/users/getTaxContributors/" + userId;

		Client client = Client.create();
		WebResource webResource = client.resource(path);
		ClientResponse response = webResource.get(ClientResponse.class);
		String responseString = response.getEntity(String.class);
		initializeTaxContributors(responseString);
	}

	private void initializeTaxContributors(String responseString) {
	
		Set<Long> contributors = new HashSet<>();
		try {
			JSONArray jsonArray = new JSONArray(responseString);
			System.out.println(responseString);
			taxContributors = new ArrayList<>();
			for (int i = 0; i < jsonArray.length(); i++) {
				if (i > 0) {
					taxContributor = new TaxContributor();
				}
				taxContributor = new TaxContributor();
				JSONObject object = jsonArray.getJSONObject(i);
				taxContributor.setId(object.getLong("id"));
				taxContributor.setFirstName(object.getString("firstName"));
				taxContributor.setLastName(object.getString("lastName"));
				taxContributor.setTaxName(object.getString("taxName"));
				taxContributor.setTaxContributors(object.getInt("taxContributors"));
				taxContributor.setPrice(object.getDouble("price"));
				taxContributor.setDueDate(object.getString("dueDate"));

				contributors.add(object.getLong("id"));
				taxContributors.add(taxContributor);
			}
			
			for(TaxContributor tc: taxContributors) {
				if(tc.getTaxContributors() > 0) {
					System.out.println("tax contributors number: " + tc.getTaxContributors());
					double price = tc.getPrice() / tc.getTaxContributors();
					tc.setPrice(Double.parseDouble(new DecimalFormat("##.##").format(price)));
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	


	public String findTaxByUserIdAndTaxName(Long userId, String name) {
		if (StringUtils.isEmpty(name)) {
			System.out.println("empty search string.. ");
			listUsersByUserId();
			return null;
		}
		String path = "http://localhost:8080/rest/users/findByTaxNameContributors/" + userId + "/" + name;

		Client client = Client.create();
		WebResource webResource = client.resource(path);
		ClientResponse response = webResource.get(ClientResponse.class);
		String responseString = response.getEntity(String.class);
		System.out.println("user to find: " + userId + " with tax: " + name);
		System.out.println(responseString);

		initializeTaxContributors(responseString);
		return null;
	}
	
	public int getNumberOfTaxes(Long userId) {
		String path = "http://localhost:8080/rest/users/getMyTaxesId/" + userId;

		Client client = Client.create();
		WebResource webResource = client.resource(path);
		ClientResponse response = webResource.get(ClientResponse.class);
		String number = response.getEntity(String.class);
		System.out.println(number);
		
		
		return Integer.valueOf(number);
	}
	
	public void confirmPayed(TaxContributor taxContributor) {
		System.out.println("confirming...");
		String postURL = "http://localhost:8080/rest/taxes/confirm/" + taxContributor.getId();

		System.out.println("trying to confirm: " + taxContributor.getFirstName() + " with id:" + taxContributor.getId());

		ClientConfig clientConfig = new DefaultClientConfig();
		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		Client client = Client.create(clientConfig);

		WebResource webResource = client.resource(postURL);
		ClientResponse response = webResource.type("application/json").put(ClientResponse.class, taxContributor);
		String responseString = response.getEntity(String.class);
		System.out.println(responseString);
		if (response.getStatus() != 200) {
			System.out.println("error " + response.getStatusInfo());
		}
		System.out.println("update complete.");
		
		listUsersByUserId();
	}

	public List<TaxContributor> getTaxContributors() {
		return taxContributors;
	}

	public void setTaxContributors(List<TaxContributor> taxContributors) {
		this.taxContributors = taxContributors;
	}

	public TaxContributor getTaxContributor() {
		return taxContributor;
	}

	public void setTaxContributor(TaxContributor taxContributor) {
		this.taxContributor = taxContributor;
	}

	public String getSearchString() {
		return searchString;
	}

	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}
	

}
