package com.fortech.taxesmemo.controller;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.ocpsoft.rewrite.annotation.Join;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fortech.taxesmemo.pojo.UserRole;
import com.fortech.taxesmemo.util.SessionUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

@Scope(value = "session")
@Component(value = "loginController")
@ELBeanName(value = "loginController")
@Join(path = "/", to = "/login.jsf")
public class LoginController {

	private String email;
	private String password;
	private String firstName;
	
	public String login(String email, String password) {
		String path = "http://localhost:8080/rest/users/login/" + email + "/" + password;

		System.out.println("path: " + path);

		Client client = Client.create();
		WebResource webResource = client.resource(path);
		ClientResponse response = webResource.get(ClientResponse.class);
		String responseString = response.getEntity(String.class);

		if (response.getStatus() != 200) {
			FacesContext.getCurrentInstance().addMessage("loginForm:password", new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "Incorrect email and password combination. Try again.", "Invalid credentials."));
			return null;
		}
		try {
			JSONObject jsonObject = new JSONObject(responseString);

			HttpSession session = SessionUtil.getSession();
			firstName = jsonObject.getString("firstName");
			session.setAttribute("firstName", firstName);
			session.setAttribute("lastName", jsonObject.getString("lastName"));
			session.setAttribute("email", email);
			session.setAttribute("userId", jsonObject.getLong("idUser"));
			session.setAttribute("role", jsonObject.getString("role"));

			System.out.println("user role: " + SessionUtil.getRole() + " ,id: " + SessionUtil.getUserId());
			if (SessionUtil.getRole().equals(UserRole.ADMIN.getRole())) {
				return "/admin/admin-taxes.xhtml?faces-redirect=true";
			}
			return "/user/user-taxes.xhtml?faces-redirect=true";
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "login";
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
