package com.fortech.taxesmemo.controller;

import org.ocpsoft.rewrite.annotation.Join;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.springframework.stereotype.Component;

import com.fortech.taxesmemo.entities.Tax;
import com.fortech.taxesmemo.entities.User;
import com.fortech.taxesmemo.pojo.UserRole;
import com.fortech.taxesmemo.util.PasswordUtil;
import com.fortech.taxesmemo.util.SessionUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;

@Component(value = "userRegisterController")
@ELBeanName(value = "userRegisterController")
@Join(path = "/registerUser", to = "/admin/user-form.jsf")
public class UserRegisterController extends AdminController {

	private User user = new User();
	private String confirmPassword;

	public String saveUser(User user) {

		user.setPassword(PasswordUtil.hashPassword(user.getPassword()));
		System.out.println(user.getFirstName());
		user.setRole(UserRole.USER.name());
		
		for(Tax t: getTaxes()) {
			user.addTax(t);
		}

		System.out.println("user role: " + SessionUtil.getRole());

		String postURL = "http://localhost:8080/rest/users/create";

		ClientConfig clientConfig = new DefaultClientConfig();
		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		Client client = Client.create(clientConfig);

		WebResource webResourcePost = client.resource(postURL);
		webResourcePost.type("application/json").post(ClientResponse.class, user);

		return "/admin/user-list.xhtml?faces-redirect=true";
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
