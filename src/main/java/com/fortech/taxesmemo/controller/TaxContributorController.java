package com.fortech.taxesmemo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ViewScoped;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.ocpsoft.rewrite.annotation.Join;
import org.ocpsoft.rewrite.annotation.RequestAction;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.ocpsoft.rewrite.faces.annotation.Deferred;
import org.ocpsoft.rewrite.faces.annotation.IgnorePostback;
import org.springframework.stereotype.Component;

import com.fortech.taxesmemo.entities.Tax;
import com.fortech.taxesmemo.entities.User;
import com.fortech.taxesmemo.util.SessionUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

@ViewScoped
@Component(value = "taxContributorController")
@ELBeanName(value = "taxContributorController")
@Join(path = "/taxContributors", to = "/admin/tax-contributors.jsf")
public class TaxContributorController {

	private List<User> users;
	private User user;
	private Long userId;

	@Deferred
	@RequestAction
	@IgnorePostback
	public void listUsersByUserId() {
		userId = SessionUtil.getUserId();
		System.out.println("userId : " + userId);

		String path = "http://localhost:8080/rest/users/myTaxContributors/" + userId;

		Client client = Client.create();
		WebResource webResource = client.resource(path);
		ClientResponse response = webResource.get(ClientResponse.class);
		String responseString = response.getEntity(String.class);
		initializeUsers(responseString);
	}

	private void initializeUsers(String responseString) {

		try {
			JSONArray jsonArray = new JSONArray(responseString);
			System.out.println(responseString);
			users = new ArrayList<>();
			for (int i = 0; i < jsonArray.length(); i++) {
				user = new User();
				JSONObject object = jsonArray.getJSONObject(i);
				user.setIdUser(object.getLong("idUser"));
				user.setFirstName(object.getString("firstName"));
				user.setLastName(object.getString("lastName"));

				if (user.getIdUser() != this.userId) {
					users.add(user);
				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public String deleteUser(User user) {
		String path = "http://localhost:8080/rest/users/delete/" + user.getIdUser();

		System.out.println("to delete tax with id: " + user.getIdUser());

		Client client = Client.create();
		WebResource webResource = client.resource(path);
		webResource.delete(ClientResponse.class);

		listUsersByUserId();

		return null;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

}
