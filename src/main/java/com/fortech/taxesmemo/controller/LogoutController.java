package com.fortech.taxesmemo.controller;

import javax.servlet.http.HttpSession;

import org.ocpsoft.rewrite.annotation.Join;
import org.ocpsoft.rewrite.annotation.RequestAction;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.ocpsoft.rewrite.faces.annotation.Deferred;
import org.ocpsoft.rewrite.faces.annotation.IgnorePostback;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fortech.taxesmemo.util.SessionUtil;

@Scope(value = "session")
@Component(value = "logoutController")
@ELBeanName(value = "logoutController")
@Join(path = "/logout", to = "/login.jsf")
public class LogoutController {

	@Deferred
	@RequestAction
	@IgnorePostback
	public String logout() {
		HttpSession session = SessionUtil.getSession();
		session.invalidate();
		return "login";
	}
}
