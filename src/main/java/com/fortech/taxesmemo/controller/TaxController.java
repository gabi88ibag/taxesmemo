package com.fortech.taxesmemo.controller;

import java.io.Serializable;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.faces.bean.ViewScoped;

import org.ocpsoft.rewrite.annotation.Join;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.springframework.stereotype.Component;
import com.fortech.taxesmemo.entities.Tax;
import com.fortech.taxesmemo.util.SessionUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;

@ViewScoped
@Component(value = "taxController")
@ELBeanName(value = "taxController")
@Join(path = "/tax", to = "/tax/tax-form.jsf")
public class TaxController {

	private Tax tax = new Tax();
	private Date dueDate;
	private Date today;

	public String save() {
		this.today = new Date();
		Format format = new SimpleDateFormat("dd/MM/yyyy");

		tax.setDueDate(format.format(dueDate));
		System.out.println("tax due date: " + tax.getDueDate());
		System.out.println("tax id: " + tax.getId());
		if (tax.getId() != null) {
			return updateTax();
		} else {
			Long userId = SessionUtil.getUserId();

			System.out.println("user role: " + SessionUtil.getRole());

			String postURL = "http://localhost:8080/rest/taxes/create/" + userId;

			ClientConfig clientConfig = new DefaultClientConfig();
			clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
			Client client = Client.create(clientConfig);

			WebResource webResourcePost = client.resource(postURL);
			webResourcePost.type("application/json").post(ClientResponse.class, tax);

			this.tax = new Tax();
			this.dueDate = null;

			return "/admin/admin-taxes.xhtml?faces-redirect=true";
		}
	}

	public String updateTax() {
		String putUrl = "http://localhost:8080/rest/taxes/update/" + tax.getId();

		System.out.println("trying to edit: " + tax.getName() + " with id:" + tax.getId());

		ClientConfig clientConfig = new DefaultClientConfig();
		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		Client client = Client.create(clientConfig);

		WebResource webResource = client.resource(putUrl);
		ClientResponse response = webResource.type("application/json").put(ClientResponse.class, tax);
		String responseString = response.getEntity(String.class);
		System.out.println(responseString);
		if (response.getStatus() != 200) {
			System.out.println("error " + response.getStatusInfo());
		}

		this.tax.setName(tax.getName());
		this.tax.setDueDate(tax.getDueDate());
		this.tax.setPrice(tax.getPrice());
		System.out.println("tax name: " + tax.getName());
		System.out.println(responseString);

		this.tax = new Tax();
		this.dueDate = null;

		return "/admin/admin-taxes.xhtml?faces-redirect=true";
	}

	public String editTax(Tax tax) {
		this.tax = tax;

		return "/tax/tax-form.xhtml?faces-redirect=true";
	}

	public Tax getTax() {
		return tax;
	}

	public void setTax(Tax tax) {
		this.tax = tax;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public Date getToday() {
		return today;
	}

	public void setToday(Date today) {
		this.today = today;
	}

}
