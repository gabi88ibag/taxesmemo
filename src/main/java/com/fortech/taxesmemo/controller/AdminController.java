package com.fortech.taxesmemo.controller;

import java.util.ArrayList;
import java.util.Collection;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.ocpsoft.rewrite.annotation.Join;
import org.ocpsoft.rewrite.annotation.RequestAction;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.ocpsoft.rewrite.faces.annotation.Deferred;
import org.ocpsoft.rewrite.faces.annotation.IgnorePostback;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.fortech.taxesmemo.entities.Tax;
import com.fortech.taxesmemo.entities.User;
import com.fortech.taxesmemo.util.SessionUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;

@Scope(value = "session")
@Component(value = "adminController")
@ELBeanName(value = "adminController")
@Join(path = "/admin", to = "/admin/admin-taxes.jsf")
public class AdminController {

	private Collection<Tax> taxes;
	private String searchString;
	private Long userId;
	private Tax tax;
	private User account;

	@Deferred
	@RequestAction
	@IgnorePostback
	public Collection<Tax> listTaxesByUserId() {
		userId = SessionUtil.getUserId();

		System.out.println("userId is : " + userId + " role: " + SessionUtil.getRole());

		String path = "http://localhost:8080/rest/taxes/getTaxesByUserId/" + userId;
		Client client = Client.create();
		WebResource webResource = client.resource(path);
		ClientResponse response = webResource.get(ClientResponse.class);
		String responseString = response.getEntity(String.class);
		System.out.println("response string " + responseString);

		try {
			JSONArray jsonArray = new JSONArray(responseString);
			System.out.println("jsonArray: " + jsonArray.toString());
			taxes = new ArrayList<>();
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject object = jsonArray.getJSONObject(i);
				Tax tax = new Tax();

				tax.setId(object.getLong("id"));
				tax.setName(object.getString("name"));
				tax.setPrice(object.getDouble("price"));
				tax.setDueDate(object.getString("dueDate"));

				taxes.add(tax);
			}
		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		return taxes;
	}

	@Deferred
	@RequestAction
	@IgnorePostback
	public void getMyAccount() {
		System.out.println("initializing user details..");
		if(account == null) {
			account = new User();
		}
		account = SessionUtil.getAccountDetails();
		System.out.println(account.getIdUser());
	}

	public Collection<Tax> getTaxes() {
		return taxes;
	}

	public String delete(Tax tax) {
		String path = "http://localhost:8080/rest/taxes/delete/" + tax.getId();

		System.out.println("to delete tax with id: " + tax.getId());

		Client client = Client.create();
		WebResource webResource = client.resource(path);
		webResource.delete(ClientResponse.class);

		listTaxesByUserId();

		return null;
	}

	public String findTaxByUserIdAndTaxName(Long userId, String name) {
		if (StringUtils.isEmpty(name)) {
			System.out.println("empty search string.. ");
			listTaxesByUserId();
			return null;
		}
		String path = "http://localhost:8080/rest/taxes/getByUserIdAndTaxName/" + userId + "/" + name;

		Client client = Client.create();
		WebResource webResource = client.resource(path);
		ClientResponse response = webResource.get(ClientResponse.class);
		String responseString = response.getEntity(String.class);
		System.out.println("user to find: " + userId + " with tax: " + name);

		taxes = new ArrayList<>();
		try {
			JSONArray jsonArray = new JSONArray(responseString);
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject object = jsonArray.getJSONObject(i);
				Tax myTax = new Tax();
				System.out.println("json obj: " + object.toString());
				myTax.setId(object.getLong("id"));
				myTax.setName(object.getString("name"));
				myTax.setPrice(object.getDouble("price"));
				myTax.setDueDate(object.getString("dueDate"));

				taxes.add(myTax);
			}
			System.out.println("new list size: " + taxes.size());
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return null;
	}

	public String saveAccount(User user) {
		SessionUtil.setFirstName(user.getFirstName());
		SessionUtil.setLastName(user.getLastName());
		SessionUtil.setEmail(user.getEmail());

		String putURL = "http://localhost:8080/rest/users/update/" + user.getIdUser();

		ClientConfig clientConfig = new DefaultClientConfig();
		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		Client client = Client.create(clientConfig);

		WebResource webResourcePost = client.resource(putURL);
		ClientResponse response = webResourcePost.type("application/json").put(ClientResponse.class, account);
		
		System.out.println(response);

		return "/admin/admin-taxes.xhtml?faces-redirect=true";
	}

	public String getSearchString() {
		return searchString;
	}

	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}

	public void setTaxes(Collection<Tax> taxes) {
		this.taxes = taxes;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Tax getTax() {
		return tax;
	}

	public void setTax(Tax tax) {
		this.tax = tax;
	}

	public User getAccount() {
		return account;
	}

	public void setAccount(User account) {
		this.account = account;
	}

}
