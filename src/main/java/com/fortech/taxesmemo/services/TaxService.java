package com.fortech.taxesmemo.services;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fortech.taxesmemo.dao.TaxDAO;
import com.fortech.taxesmemo.dao.UserDAO;
import com.fortech.taxesmemo.entities.Tax;

@Service
@Transactional
public class TaxService {

	@Autowired
	TaxDAO taxDAO;

	@Autowired
	UserDAO userDAO;

	public Optional<Tax> get(Long byId) {
		return taxDAO.findById(byId);
	}

	public List<Tax> findByName(String name) {
		return taxDAO.findByName(name);
	}

	public List<Tax> findTaxesByUserId(Long id) {

		return taxDAO.findTaxByIdUser(id);
	}

	public Collection<Tax> list() {
		return taxDAO.findAll();
	}

	@Transactional
	public Tax save(Tax tax) {
		return taxDAO.save(tax);
	}

	@Transactional
	public void delete(Tax tax) {
		taxDAO.delete(tax);
	}

	/**
	 * @param taxRepository
	 */
	public TaxService(TaxDAO taxDAO) {
		super();
		this.taxDAO = taxDAO;
	}

	public TaxDAO getTaxDAO() {
		return taxDAO;
	}

	public void setTaxDAO(TaxDAO taxDAO) {
		this.taxDAO = taxDAO;
	}

	/**
	 * @param taxDAO
	 * @param userDAO
	 */
	public TaxService(TaxDAO taxDAO, UserDAO userDAO) {
		super();
		this.taxDAO = taxDAO;
		this.userDAO = userDAO;
	}

	/**
	 * 
	 */
	public TaxService() {
		super();
	}
	
	
}
