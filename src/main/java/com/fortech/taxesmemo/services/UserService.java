package com.fortech.taxesmemo.services;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fortech.taxesmemo.dao.UserDAO;
import com.fortech.taxesmemo.entities.Tax;
import com.fortech.taxesmemo.entities.User;
import com.fortech.taxesmemo.pojo.TaxContributor;

@Service
@Transactional
public class UserService {
	@Autowired
	private UserDAO userDAO;

	public Optional<User> findById(Long byId) {
		return userDAO.findById(byId);
	}

	public Optional<User> findUserByEmail(String email, String password) {
		return userDAO.findUserByEmail(email, password);
	}

	public boolean findbyEmail(String email) {
		return userDAO.findbyEmail(email);
	}
	
	public Optional<User> getByEmail(String email) {
		return userDAO.getByEmail(email);
	}

	public List<Tax> findTaxesByUserId(Long id) {
		return userDAO.findTaxesByUserId(id);
	}

	public List<Tax> findByUserIdAndTaxName(Long id, String name) {
		return userDAO.findByUserIdAndTaxName(id, name);
	}

	public List<TaxContributor> getTaxContributors(Long userId) {
		return userDAO.getTaxContributors(userId);
	}

	public List<TaxContributor> getTaxContributorsByName(Long userId, String name) {
		return userDAO.getTaxContributorsByName(userId, name);
	}

	public Collection<User> findAll() {
		return userDAO.findAll();
	}

	public User save(User user) {
		return userDAO.save(user);
	}

	@Transactional
	public void delete(User user) {
		userDAO.delete(user);
	}

	public void saveTax(Long userId, Tax tax) {
		userDAO.saveTax(userId, tax);
	}

	public void updatePayed(Long taxId, Long id) {
		userDAO.updatePayed(taxId, id);
	}

	/**
	 * 
	 */
	public UserService() {
		super();
	}

	public List<User> getMyTaxContributors(Long id) {
		return userDAO.getMyTaxContributors(id);
	}

	public void update(@Valid User user) {
		userDAO.update(user);
	}

}
