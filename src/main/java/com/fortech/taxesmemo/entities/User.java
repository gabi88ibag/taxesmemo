package com.fortech.taxesmemo.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonProperty;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 * This class represents a User which has an id, first name, last name, role
 * email and a list of taxes.
 * 
 * @author Gabriel Ciurdas created on May 4, 2018
 */
@Entity(name = "User")
@Table(name = "user")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_user")
	@JsonProperty("id")
	private Long idUser;

	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH })
	@JoinTable(name = "user_tax", joinColumns = { @JoinColumn(name = "id_user") }, inverseJoinColumns = {
			@JoinColumn(name = "id_tax") })
	@JsonBackReference
	private Set<Tax> taxes = new HashSet<>();

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "role")
	private String role;

	@Column(name = "email")
	private String email;

	@Column(name = "password")
	private String password;

	/**
	 * Constructs a User object with given id, first name, last name, email and
	 * password.
	 * 
	 * @param id
	 *            is the user's id
	 * @param firstName
	 *            is user's first name
	 * @param lastName
	 *            is user's last name
	 * @param role
	 *            is the user's role
	 * @param email
	 *            is user's email
	 * @param password
	 *            is the user's password
	 */
	public User(Long id, String firstName, String lastName, String role, String email, String password) {
		this.idUser = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.role = role;
		this.email = email;
		this.password = password;
	}

	/**
	 * Constructs a User object with given first name, last name, email and
	 * password.
	 * 
	 * @param firstName
	 *            is user's first name
	 * @param lastName
	 *            is user's last name
	 * @param email
	 *            is user's email
	 * @param password
	 *            is the user's password
	 */
	public User(String firstName, String lastName, String role, String email, String password) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
	}

	public User() {

	}

	public void addTax(Tax tax) {
		if (!taxes.contains(tax)) {
			System.out.println("tax added: " + tax.toString());
			taxes.add(tax);
			tax.getUsers().add(this);
		}
	}

	public void removeTax(Tax tax) {
		taxes.remove(tax);
		tax.getUsers().remove(this);
	}

	public Long getIdUser() {
		return idUser;
	}

	public void setIdUser(Long id) {
		this.idUser = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Set<Tax> getTaxes() {
		return taxes;
	}

	public void setTaxes(Set<Tax> taxes) {
		this.taxes = taxes;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof User))
			return false;
		return idUser != null && idUser.equals(((User) o).idUser);
	}

	@Override
	public int hashCode() {
		return 31;
	}

	@Override
	public String toString() {
		return "User [idUser=" + idUser + ", taxes=not included, firstName=" + firstName + ", lastName=" + lastName
				+ ", role=" + role + ", email=" + email + ", password=" + password + "]";
	}

}
