package com.fortech.taxesmemo.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * This class represents a Tax which has an id, name, price and due date.
 * 
 * @author Gabriel Ciurdas created on May 4, 2018
 */
@Entity
@Table(name = "tax")
public class Tax {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_tax")
	@JsonProperty("id")
	private Long id;

	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "taxes", cascade = CascadeType.MERGE)
	@JsonIgnore
	private Set<User> users = new HashSet<>();

	@Column(name = "tax_name")
	@JsonProperty("name")
	private String name;

	@Column(name = "price")
	private double price;

	@Column(name = "tax_due_date")
	@JsonProperty("dueDate")
	private String dueDate;

	/**
	 * Constructs a Tax object with given id, name, amount and due date.
	 * 
	 * @param id
	 *            is the id of the tax
	 * @param name
	 *            is the name of the tax
	 * @param price
	 *            is the price of the tax
	 * @param dueDate
	 *            is the due date to pay the tax
	 */
	public Tax(Long id, String name, double price, String dueDate) {
		this.id = id;
		this.name = name;
		this.price = price;
		this.dueDate = dueDate;
	}

	/**
	 * Constructs a Tax object with given name, amount and due date.
	 * 
	 * @param name
	 *            is the name of the tax
	 * @param price
	 *            is the price of the tax
	 * @param dueDate
	 *            is the due date to pay the tax
	 */
	public Tax(String name, double price, String dueDate) {
		this.name = name;
		this.price = price;
		this.dueDate = dueDate;
	}

	public Tax() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	public void addUser(User user) {
		if (!users.contains(user)) {
			users.add(user);
			user.getTaxes().add(this);
		}
	}

	public void removeUser(User user) {
		users.remove(user);
		user.getTaxes().remove(this);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof Tax))
			return false;
		return id != null && id.equals(((Tax) o).id);
	}

	@Override
	public int hashCode() {
		return 31;
	}

	@Override
	public String toString() {
		return "Tax [id=" + id + ", users=not included, name=" + name + ", price=" + price + ", dueDate=" + dueDate
				+ "]";
	}
	
}
