package com.fortech.taxesmemo.rest;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fortech.taxesmemo.entities.User;
import com.fortech.taxesmemo.pojo.TaxContributor;
import com.fortech.taxesmemo.services.UserService;
import com.fortech.taxesmemo.util.PasswordUtil;

@RestController
@RequestMapping("/rest/users")
public class UserResource {

	@Autowired
	UserService userService;

	@GetMapping("/all")
	public List<User> getAllUsers() {
		return (List<User>) userService.findAll();
	}

	@GetMapping("/myTaxContributors/{id}")
	public List<User> getMyContributors(@PathVariable("id") final Long id) {
		return (List<User>) userService.getMyTaxContributors(id);
	}

	@GetMapping("{id}")
	public ResponseEntity<User> getUserById(@PathVariable("id") final Long id) {
		Optional<User> optionalUser = userService.findById(id);
		User user = optionalUser.orElse(new User());

		if (optionalUser.isPresent()) {
			return ResponseEntity.ok().body(user);
		}
		return ResponseEntity.notFound().build();
	}

	@GetMapping("login/{email}/{password}")
	public ResponseEntity<User> getUserByEmail(@PathVariable("email") String email,
			@PathVariable("password") String password) {
		Optional<User> optionalUser = userService.getByEmail(email);
		User user = optionalUser.orElse(new User());

		if (optionalUser.isPresent()) {
			System.out.println("cheking pass: " + password + " with " + user.getPassword());
			if (PasswordUtil.checkPassword(password, user.getPassword())) {
				return ResponseEntity.ok().body(user);
			}
		}

		return ResponseEntity.notFound().build();
	}

	@PostMapping("/create")
	public User createUser(@Valid @RequestBody User user) {
		System.out.println("to save: " + user.toString());
		return userService.save(user);
	}

	@PutMapping("/update/{id}")
	public ResponseEntity<User> updateUser(@PathVariable(value = "id") Long id, @Valid @RequestBody User user) {
		Optional<User> optionalUser = userService.findById(id);

		if (optionalUser.isPresent()) {
			User u = optionalUser.get();
			user.setIdUser(u.getIdUser());
			user.setRole(u.getRole());
			user.setPassword(u.getPassword());
			user.setTaxes(u.getTaxes());
			userService.update(user);
			return ResponseEntity.ok().body(user);
		}

		return ResponseEntity.ok().body(user);
	}

	@GetMapping("/getTaxContributors/{id}")
	public ResponseEntity<List<TaxContributor>> getTaxContributors(@PathVariable(value = "id") Long id) {

		List<TaxContributor> taxContributors = userService.getTaxContributors(id);

		return ResponseEntity.ok().body(taxContributors);

	}

	@GetMapping("/findByTaxNameContributors/{id}/{name}")
	public ResponseEntity<List<TaxContributor>> findByTaxNameContributors(@PathVariable(value = "id") Long id,
			@PathVariable(value = "name") String name) {

		List<TaxContributor> taxContributors = userService.getTaxContributorsByName(id, name);

		return ResponseEntity.ok().body(taxContributors);

	}

	@DeleteMapping("/delete/{id}")
	public ResponseEntity<User> deleteUser(@PathVariable(value = "id") Long id) {

		Optional<User> optionalUser = userService.findById(id);
		User user = optionalUser.orElse(new User());

		if (optionalUser.isPresent()) {
			userService.delete(user);
			return ResponseEntity.ok().body(user);
		}

		return ResponseEntity.ok().body(user);

	}
}
