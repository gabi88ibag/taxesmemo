package com.fortech.taxesmemo.rest;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fortech.taxesmemo.entities.Tax;
import com.fortech.taxesmemo.pojo.TaxContributor;
import com.fortech.taxesmemo.services.TaxService;
import com.fortech.taxesmemo.services.UserService;

@RestController
@RequestMapping("/rest/taxes")
public class TaxResource {

	@Autowired
	TaxService taxService;

	@Autowired
	UserService userService;

	@GetMapping("/all")
	public List<Tax> getAllTaxes() {
		return (List<Tax>) taxService.list();
	}

	@GetMapping("{id}")
	public ResponseEntity<Tax> getTaxById(@PathVariable("id") final Long id) {
		Optional<Tax> optionalTax = taxService.get(id);
		Tax tax = optionalTax.orElse(new Tax());

		if (optionalTax.isPresent()) {
			return ResponseEntity.ok().body(tax);
		}
		return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
	}

	@GetMapping("/getTaxesByUserId/{id}")
	public ResponseEntity<List<Tax>> getTaxesByUserId(@PathVariable("id") Long id) {
		List<Tax> taxes = userService.findTaxesByUserId(id);
		return ResponseEntity.ok().body(taxes);
	}

	@GetMapping("getByName/{name}")
	public ResponseEntity<List<Tax>> getTaxByName(@PathVariable("name") final String name) {
		List<Tax> taxes = taxService.findByName(name);

		return ResponseEntity.ok().body(taxes);
	}

	@GetMapping("getByUserIdAndTaxName/{userId}/{name}")
	public ResponseEntity<List<Tax>> getTaxByName(@PathVariable("userId") Long userId,
			@PathVariable("name") String name) {

		List<Tax> taxes = userService.findByUserIdAndTaxName(userId, name);

		return ResponseEntity.ok().body(taxes);
	}

	@PostMapping("/create/{id}")
	public void createTax(@Valid @RequestBody Tax tax, @PathVariable(value = "id") Long id) {
		System.out.println("saving tax:" + tax + " for user " + id);
		userService.saveTax(id, tax);
	}

	@PutMapping("/update/{id}")
	public ResponseEntity<Tax> updateTax(@PathVariable(value = "id") Long id, @Valid @RequestBody Tax tax) {
		Optional<Tax> optionalTax = taxService.get(id);
		Tax myTax = optionalTax.orElse(new Tax());

		if (optionalTax.isPresent()) {
			myTax.setName(tax.getName());
			myTax.setPrice(tax.getPrice());
			myTax.setName(tax.getName());
			myTax.setDueDate(tax.getDueDate());

			Tax updatedTax = taxService.save(myTax);
			return ResponseEntity.ok().body(updatedTax);
		}

		return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
	}

	@PutMapping("/confirm/{id}")
	public ResponseEntity<TaxContributor> confirmPayed(@PathVariable(value = "id") Long id,
			@Valid @RequestBody TaxContributor contributor) {

		List<Tax> tax = taxService.findByName(contributor.getTaxName());
		Long taxId;
		if (tax.size() > 0) {
			taxId = tax.get(0).getId();
			System.out.println("tax id: ->" + taxId);
			userService.updatePayed(taxId, contributor.getId());
			return ResponseEntity.ok().body(contributor);
		}

		return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
	}

	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Tax> deleteTax(@PathVariable(value = "id") Long id) {

		Optional<Tax> optionalTax = taxService.get(id);
		Tax tax = optionalTax.orElse(new Tax());

		if (optionalTax.isPresent()) {
			taxService.delete(tax);
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.notFound().build();

	}
}
