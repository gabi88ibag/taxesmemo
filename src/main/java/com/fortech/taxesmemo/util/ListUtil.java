package com.fortech.taxesmemo.util;

import java.util.List;

public class ListUtil {

	public static String separateWithComma(List<Long> taxesId) {
		String taxes = "";
		for (int i = 0; i < taxesId.size(); i++) {
			if (i == taxesId.size() - 1) {
				taxes = taxes + String.valueOf(taxesId.get(i));
			} else {
				taxes = taxes + taxesId.get(i) + ",";
			}
		}
		return taxes;
	}
}
