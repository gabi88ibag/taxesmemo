package com.fortech.taxesmemo.util;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.fortech.taxesmemo.entities.User;

public class SessionUtil {

	public static HttpSession getSession() {
		return (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
	}

	public static HttpServletRequest getRequest() {
		return (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	}

	public static String getEmail() {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		return session.getAttribute("email").toString();
	}

	public static Long getUserId() {
		HttpSession session = getSession();
		return (Long) session.getAttribute("userId");
	}

	public static String getRole() {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		return (String) session.getAttribute("role");
	}

	public static User getAccountDetails() {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		User user = new User();

		user.setIdUser(getUserId());
		if (null != session) {
			user.setFirstName(session.getAttribute("firstName").toString());
			user.setLastName(session.getAttribute("lastName").toString());
			user.setEmail(getEmail());
		}

		return user;
	}

	public static void setFirstName(String firstName) {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		session.setAttribute("firstName", firstName);
	}

	public static void setLastName(String lastName) {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		session.setAttribute("lastName", lastName);
	}

	public static void setEmail(String email) {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		session.setAttribute("email", email);
	}

	public static void setNumberOfTaxes(int size) {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		session.setAttribute("taxesNumber", size);
	}

	public static int getTaxesNumber() {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		int size = (int) session.getAttribute("taxNumber");
		return size;
	}
}
