package com.fortech.taxesmemo.util;

import org.mindrot.jbcrypt.BCrypt;

public class PasswordUtil {

	public static String hashPassword(String plainTextPassword) {
		return BCrypt.hashpw(plainTextPassword, BCrypt.gensalt());
	}

	public static boolean checkPassword(String password, String hashPassword) {
		return BCrypt.checkpw(password, hashPassword);
	}
}
