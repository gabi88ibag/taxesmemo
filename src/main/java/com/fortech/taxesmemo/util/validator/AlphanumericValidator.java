package com.fortech.taxesmemo.util.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AlphanumericValidator {
	private static final Pattern VALID_ALPHANUMERIC_CHARACTERS = Pattern.compile("^[a-zA-Z0-9]+$");
	private static final Pattern VALID_ALPHA_CHARACTERS = Pattern.compile("^[a-zA-Z]+$");
	private static final Pattern VALID_NUMERIC_CHARACTERS = Pattern.compile("-?\\d+(\\.\\d+)?");
	
	public static boolean isAlphaNumerical(String field) {
		return match(field.replaceAll("\\s+", ""), VALID_ALPHANUMERIC_CHARACTERS);
	}

	public static boolean isAlphabetical(String field) {
		return match(field, VALID_ALPHA_CHARACTERS);
	}

	public static boolean isNumerical(String field) {
		return match(field, VALID_NUMERIC_CHARACTERS);
	}

	private static boolean match(String field, Pattern pattern) {
		Matcher matcher = pattern.matcher(field);
		if (matcher.find()) {
			return true;
		}
		return false;
	}
}
