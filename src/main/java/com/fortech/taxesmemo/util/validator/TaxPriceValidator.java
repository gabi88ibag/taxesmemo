package com.fortech.taxesmemo.util.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("request")
public class TaxPriceValidator implements Validator {

	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		if (value.toString() == null || value.toString().length() == 0) {
			FacesMessage msg = new FacesMessage("Tax Price cannot be empty.", "You must enter a Tax Price.");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);

			throw new ValidatorException(msg);
		} else if ((double) value < 0.01) {
			FacesMessage msg = new FacesMessage("Tax Price must be at least 0.01",
					"Please enter a price greater than 0.01.");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);

			throw new ValidatorException(msg);
		} else if (!AlphanumericValidator.isNumerical(value.toString())) {
			FacesMessage msg = new FacesMessage("Tax Price can only be composed of numbers.",
					"Please enter a price in numbers.");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);

			throw new ValidatorException(msg);
		}
	}

}
