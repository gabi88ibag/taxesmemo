package com.fortech.taxesmemo.util.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("request")
public class TaxNameValidator implements Validator {

	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		if (value.toString() == null || value.toString().length() == 0) {
			FacesMessage msg = new FacesMessage("Tax Name cannot be empty.", "You must enter a Tax Name.");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);

			throw new ValidatorException(msg);
		} else if (!AlphanumericValidator.isAlphaNumerical(value.toString())) {
			FacesMessage msg = new FacesMessage("Tax Name can only be composed of alphanumerical characters.",
					"Tax Nam can only be composed of letters or numbers");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);

			throw new ValidatorException(msg);
		}
	}

}
