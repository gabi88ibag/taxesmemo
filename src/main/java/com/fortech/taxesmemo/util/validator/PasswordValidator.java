package com.fortech.taxesmemo.util.validator;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.application.ViewHandler;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class PasswordValidator implements Validator {

	private static final int PASSWORD_MIN_LENGTH = 6;

	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		if (value.toString() == null || value.toString().length() == 0) {
			FacesMessage msg = new FacesMessage("Password cannot be empty.", "Please enter your Password.");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);

			throw new ValidatorException(msg);
		} else if (value.toString().length() < PASSWORD_MIN_LENGTH) {
			FacesMessage msg = new FacesMessage("Password must be composed of at least 6 characters.",
					"Please provide a password of at least 6 characters long.");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);

			throw new ValidatorException(msg);
		}
	}

	public void refresh() {
		FacesContext context = FacesContext.getCurrentInstance();
		Application application = context.getApplication();
		ViewHandler viewHandler = application.getViewHandler();
		UIViewRoot viewRoot = viewHandler.createView(context, context.getViewRoot().getViewId());
		context.setViewRoot(viewRoot);
	}

}
