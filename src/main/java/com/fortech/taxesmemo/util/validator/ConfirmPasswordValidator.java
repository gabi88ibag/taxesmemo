package com.fortech.taxesmemo.util.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("request")
public class ConfirmPasswordValidator implements Validator {

	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		UIInput passwordInput = (UIInput) component.findComponent("password");
		String password = (String) passwordInput.getLocalValue();

		if (value.toString() == null || value.toString().length() == 0) {
			FacesMessage msg = new FacesMessage("Confirm Password cannot be empty.", "Please enter Confirm Password.");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);

			throw new ValidatorException(msg);
		} else if (!password.equals(value.toString())) {
			FacesMessage msg = new FacesMessage("Confirm password does not match Password.",
					"You retype your Password in Confirm Password field.");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);
		}
	}

}
