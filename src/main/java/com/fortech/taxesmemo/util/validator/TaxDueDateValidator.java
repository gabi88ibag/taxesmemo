package com.fortech.taxesmemo.util.validator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("request")
public class TaxDueDateValidator implements Validator {

	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat dueDateFormatter = new SimpleDateFormat("EE MMM dd HH:mm:ss z yyyy", Locale.ENGLISH);
		
		Date today = new Date();
		Date dueDate = null;	
		try {
			today = formatter.parse(formatter.format(today));
			dueDate = dueDateFormatter.parse(value.toString());
			dueDate = formatter.parse(formatter.format(dueDate));
		} catch (ParseException e) {
			e.printStackTrace();
		}

		if (value.toString() == null || value.toString().length() == 0) {
			FacesMessage msg = new FacesMessage("Tax Due Date cannot be empty.", "You must enter a Tax Due Date.");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);

			throw new ValidatorException(msg);
		} else if (dueDate.before(today) && !dueDate.equals(today)) {
			FacesMessage msg = new FacesMessage("Tax Due Date cannot be a day from the past.",
					"Tax Due Date can only be a future day or the current day.");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);

			throw new ValidatorException(msg);
		}

	}

}
