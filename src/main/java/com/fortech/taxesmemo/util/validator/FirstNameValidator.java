package com.fortech.taxesmemo.util.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("request")
public class FirstNameValidator implements Validator {

	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		if (value.toString() == null || value.toString().length() == 0) {
			FacesMessage msg = new FacesMessage("First Name cannot be empty.", "You must enter your First Name.");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);

			throw new ValidatorException(msg);
		} else if (!AlphanumericValidator.isAlphabetical(value.toString())) {
			FacesMessage msg = new FacesMessage("First Name can only be composed of alphabetical characters.",
					"First Name and Last Name can only be composed of alphabetical characters.");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);

			throw new ValidatorException(msg);
		}
	}

}
