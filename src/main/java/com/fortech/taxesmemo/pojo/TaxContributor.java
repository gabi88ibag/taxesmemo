package com.fortech.taxesmemo.pojo;

public class TaxContributor {
	private Long id;
	private String firstName;
	private String lastName;
	private String taxName;
	private int taxContributors;
	private double price;
	private String dueDate;

	/**
	 * @param id
	 * @param firstName
	 * @param lastName
	 * @param taxName
	 * @param price
	 * @param dueDate
	 */
	public TaxContributor(Long id, String firstName, String lastName, String taxName, int taxContributors, double price,
			String dueDate) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.taxName = taxName;
		this.taxContributors = taxContributors;
		this.price = price;
		this.dueDate = dueDate;
	}

	public TaxContributor() {
		this.id = 0L;
		this.firstName = "";
		this.lastName = "";
		this.taxName = "";
		this.taxContributors = 0;
		this.price = 0;
		this.dueDate = "";
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getTaxName() {
		return taxName;
	}

	public void setTaxName(String taxName) {
		this.taxName = taxName;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getTaxContributors() {
		return taxContributors;
	}

	public void setTaxContributors(int taxContributors) {
		this.taxContributors = taxContributors;
	}

}
